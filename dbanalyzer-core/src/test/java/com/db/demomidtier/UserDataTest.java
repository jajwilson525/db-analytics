package com.db.demomidtier;

import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.sql.*;

import static com.db.demomidtier.UserData.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class UserDataTest {

    @Mock
    private Connection jdbcConnection;

    @Mock
    private ResultSet resultSet;

    @Mock
    private Statement statement;

    @Mock
    private PreparedStatement preparedStatement;

    @Mock
    private ResultSetMetaData resultSetMetaData;

    private static final String orderColumn = "order";
    private static final String counterpartyName = "counterparty";
    private static final String instrumentName = "instrument";
    private static final String instrumentType = "type";
    private static final String dealerName = "dealer";
    private static final int offset = 0;
    private static final int limit = 0;
    private static final String viewName = "db_grad_cs_1917.deal_view";

    private static final String[] queries = new String[]{
            String.format(GET_AVERAGE_PRICES_QUERY, viewName),
            String.format(GET_REALIZED_PROFIT_QUERY, viewName),
            String.format(GET_INSTR_DETAILS_QUERY, viewName),
            String.format(GET_COUNTERPARTY_DETAIL_QUERY, viewName),
            String.format(GET_COUNTERPARTIES_QUERY, viewName),
            String.format(GET_INSTR_QUERY, viewName),
            String.format(GET_INSTR_PORTF_BY_COUNT, viewName, viewName),
            String.format(GET_ALL_DEALS_QUERY, viewName),
            String.format(GET_NUM_ROWS_QUERY, viewName),
            String.format(GET_REALIZED_PROFIT_PER_DEALER_QUERY, viewName),
            String.format(GET_DEALER_NET_QUANTITIES_QUERY, viewName),
            String.format(GET_AVERAGE_PRICES_PER_DEALER_QUERY, viewName),
            String.format(GET_LAST_PRICES_QUERY, viewName, viewName),
            "Select * from " + viewName + " ORDER BY " + orderColumn + " DESC" + " LIMIT " + offset + "," + limit,
            "Select * from " + viewName + " ORDER BY " + orderColumn + " LIMIT " + offset + "," + limit
    };

    private void prepareStatement() throws SQLException {
        for (String query : queries) {
            Mockito.when(jdbcConnection.prepareStatement(query)).thenReturn(preparedStatement);
        }
    }

    private void prepareFailStatement() throws SQLException {
        for (String query : queries) {
            Mockito.when(jdbcConnection.prepareStatement(query)).thenThrow(new SQLException());
        }
    }

    @Before
    public void setUp() throws Exception {
        resultSet = Mockito.mock(ResultSet.class);
        resultSetMetaData = Mockito.mock(ResultSetMetaData.class);
        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(false);
        Mockito.when(resultSet.getMetaData()).thenReturn(resultSetMetaData);
        Mockito.when(resultSet.getMetaData().getColumnCount()).thenReturn(0);

        preparedStatement = Mockito.mock(PreparedStatement.class);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);

        jdbcConnection = Mockito.mock(Connection.class);
        Mockito.when(jdbcConnection.createStatement()).thenReturn(statement);
        prepareStatement();
        Mockito.when(jdbcConnection.isValid(1)).thenReturn(true);
    }

    @Test
    public void tests_with_empty_response_from_db() throws SQLException {
        UserData userData = new UserData(jdbcConnection);

        assertThat(userData.getAllDeals(), is("[{}]"));
        assertThat(userData.getAveragePrices(), is("[]"));
        assertThat(userData.getCounterpartyDetails(""), is("[]"));
        assertThat(userData.getCounterpartyList(), is("[]"));
        assertThat(userData.getInstrumentDetails(instrumentName), is("[]"));
        assertThat(userData.getInstrumentList(), is("[]"));
        assertThat(userData.getRealizedProfits(), is("[]"));
        assertThat(userData.getInstrumentPortfolioByCounterparty(instrumentName,instrumentType), is("[]"));
        assertThat(userData.getEffectiveProfits().toString(), is("[]"));
        assertThat(userData.getDealerNetQuantities(dealerName).toString(), is("{}"));
        assertThat(userData.getAvgPrices(counterpartyName,true).toString(), is("{}"));
        assertThat(userData.getAvgPrices(counterpartyName,false).toString(), is("{}"));
        assertNull(userData.getEffectiveProfit(counterpartyName,null,null,null,null));
    }

    @Test
    public void tests_with_exception1() throws SQLException {
        Mockito.reset(preparedStatement);
        preparedStatement = Mockito.mock(PreparedStatement.class);
        Mockito.when(preparedStatement.executeQuery()).thenThrow(new SQLException());
        Mockito.reset(jdbcConnection);
        jdbcConnection = Mockito.mock(Connection.class);
        Mockito.when(jdbcConnection.createStatement()).thenReturn(statement);
        prepareStatement();
        Mockito.when(jdbcConnection.isValid(1)).thenReturn(true);

        UserData userData = new UserData(jdbcConnection);
        assertNull(userData.getAllDeals());
        assertNull(userData.getAveragePrices());
        assertNull(userData.getCounterpartyDetails(""));
        assertNull(userData.getCounterpartyList());
        assertNull(userData.getInstrumentDetails(instrumentName));
        assertNull(userData.getInstrumentList());
        assertNull(userData.getRealizedProfits());
        assertNull(userData.getInstrumentPortfolioByCounterparty(instrumentName,instrumentType));
        assertNull(userData.getEffectiveProfits());
        assertNull(userData.getDealerNetQuantities(dealerName));
        assertNull(userData.getAvgPrices(counterpartyName,true));
        assertNull(userData.getAvgPrices(counterpartyName,false));
        assertNull(userData.getEffectiveProfit(counterpartyName,null,null,null,null));
    }

    @Test(expected = SQLException.class)
    public void tests_with_exception2() throws SQLException {
        Mockito.reset(preparedStatement);
        preparedStatement = Mockito.mock(PreparedStatement.class);
        Mockito.when(preparedStatement.executeQuery()).thenThrow(new SQLException());
        Mockito.reset(jdbcConnection);
        jdbcConnection = Mockito.mock(Connection.class);
        Mockito.when(jdbcConnection.createStatement()).thenReturn(statement);
        prepareFailStatement();
        Mockito.when(jdbcConnection.isValid(1)).thenReturn(true);

        UserData userData = new UserData(jdbcConnection);
        assertNull(userData.getAllDeals());
        assertNull(userData.getAveragePrices());
        assertNull(userData.getCounterpartyDetails(""));
        assertNull(userData.getCounterpartyList());
        assertNull(userData.getInstrumentDetails(instrumentName));
        assertNull(userData.getInstrumentList());
        assertNull(userData.getRealizedProfits());
        assertNull(userData.getAveragePrices());
        assertNull(userData.getInstrumentPortfolioByCounterparty(instrumentName,instrumentType));
        assertNull(userData.getEffectiveProfits());
        assertNull(userData.getAvgPrices(counterpartyName,true));
        assertNull(userData.getAvgPrices(counterpartyName,false));
        assertNull(userData.getEffectiveProfit(counterpartyName,null,null,null,null));
        userData.getDealerNetQuantities(dealerName);
    }

    @Test
    public void test_deals_desc() throws SQLException {
        String amount_query = "select count(*) as num_of_rows from " + viewName;
        PreparedStatement amountPreparedStatement = Mockito.mock(PreparedStatement.class);
        ResultSet amountResultSet = Mockito.mock(ResultSet.class);
        ResultSetMetaData amountResultSetMetaData = Mockito.mock(ResultSetMetaData.class);
        Mockito.when(amountResultSet.next()).thenReturn(true).thenReturn(false);
        Mockito.when(amountResultSet.getMetaData()).thenReturn(amountResultSetMetaData);
        Mockito.when(amountResultSet.getMetaData().getColumnCount()).thenReturn(1);
        Mockito.when(amountResultSet.getMetaData().getColumnLabel(1)).thenReturn("num_of_rows");
        Mockito.when(amountResultSet.getObject(1)).thenReturn("0");
        Mockito.when(amountPreparedStatement.executeQuery()).thenReturn(amountResultSet);
        Mockito.when(jdbcConnection.prepareStatement(amount_query)).thenReturn(amountPreparedStatement);

        UserData userData = new UserData(jdbcConnection);
        assertThat(userData.getDealsTable(0, offset, limit, orderColumn, false).toString(), is("{\"recordsFiltered\":0,\"data\":[{}],\"draw\":0,\"recordsTotal\":0}"));
    }

    @Test
    public void test_deals_asc() throws SQLException {
        String amount_query = "select count(*) as num_of_rows from " + viewName;
        PreparedStatement amountPreparedStatement = Mockito.mock(PreparedStatement.class);
        ResultSet amountResultSet = Mockito.mock(ResultSet.class);
        ResultSetMetaData amountResultSetMetaData = Mockito.mock(ResultSetMetaData.class);
        Mockito.when(amountResultSet.next()).thenReturn(true).thenReturn(false);
        Mockito.when(amountResultSet.getMetaData()).thenReturn(amountResultSetMetaData);
        Mockito.when(amountResultSet.getMetaData().getColumnCount()).thenReturn(1);
        Mockito.when(amountResultSet.getMetaData().getColumnLabel(1)).thenReturn("num_of_rows");
        Mockito.when(amountResultSet.getObject(1)).thenReturn("0");
        Mockito.when(amountPreparedStatement.executeQuery()).thenReturn(amountResultSet);
        Mockito.when(jdbcConnection.prepareStatement(amount_query)).thenReturn(amountPreparedStatement);

        UserData userData = new UserData(jdbcConnection);
        assertThat(userData.getDealsTable(0, offset, limit, orderColumn, true).toString(), is("{\"recordsFiltered\":0,\"data\":[{}],\"draw\":0,\"recordsTotal\":0}"));
    }

    @Test
    public void test_deals_exception_in_amount() throws SQLException {
        String amount_query = "select count(*) as num_of_rows from " + viewName;
        PreparedStatement amountPreparedStatement = Mockito.mock(PreparedStatement.class);
        ResultSet amountResultSet = Mockito.mock(ResultSet.class);
        ResultSetMetaData amountResultSetMetaData = Mockito.mock(ResultSetMetaData.class);
        Mockito.when(amountResultSet.next()).thenReturn(true).thenReturn(false);
        Mockito.when(amountResultSet.getMetaData()).thenReturn(amountResultSetMetaData);
        Mockito.when(amountResultSet.getMetaData().getColumnCount()).thenReturn(1);
        Mockito.when(amountResultSet.getMetaData().getColumnLabel(1)).thenReturn("num_of_rows");
        Mockito.when(amountResultSet.getObject(1)).thenReturn("0");
        Mockito.when(amountPreparedStatement.executeQuery()).thenReturn(amountResultSet);
        Mockito.when(jdbcConnection.prepareStatement(amount_query)).thenThrow(new SQLException());
        Mockito.when(jdbcConnection.isValid(1)).thenReturn(true);

        UserData userData = new UserData(jdbcConnection);
        assertThat(userData.getDealsTable(0, offset, limit, orderColumn, true).toString(), is("{\"data\":[{}],\"draw\":0,\"error\":\"Error getting the amount of rows\"}"));
    }

    @Test
    public void test_deals_exception_in_data() throws SQLException {
        Mockito.reset(jdbcConnection);
        jdbcConnection = Mockito.mock(Connection.class);
        Mockito.when(jdbcConnection.createStatement()).thenReturn(statement);
        Mockito.when(jdbcConnection
                .prepareStatement("Select * from " + viewName + " ORDER BY " + orderColumn + " LIMIT " + offset + "," + limit))
                .thenThrow(new SQLException());
        Mockito.when(jdbcConnection.isValid(1)).thenReturn(true);
        UserData userData = new UserData(jdbcConnection);
        System.out.println(userData.getDealsTable(0, offset, limit, orderColumn, true).toString());
        assertThat(userData.getDealsTable(0, offset, limit, orderColumn, true).toString(), is("{\"draw\":0,\"error\":\"Error loading data from database\"}"));
    }
}