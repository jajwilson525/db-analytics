package com.db.demomidtier;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Properties;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

@RunWith(JUnit4.class)
public class PropertyLoaderTest {

    @Test
    public void try_to_open_not_exists_property_file() {
        PropertyLoader propertyLoader = PropertyLoader.getLoader();
        Properties p = propertyLoader.getPropValues("wrongFile.properties");
        assertThat(p,is((Properties)null));
    }

    @Test
    public void try_to_open_exists_property_file() {
        PropertyLoader propertyLoader = PropertyLoader.getLoader();
        Properties p = propertyLoader.getPropValues("dbConnector.properties");
        assertNotEquals(p, null);
    }

    @Test
    public void get_property_value(){
        PropertyLoader propertyLoader = PropertyLoader.getLoader();
        String dbDriverFromProperties = propertyLoader.getPropValues("dbConnector.properties").getProperty("dbDriver");
        assertThat(dbDriverFromProperties,is("com.mysql.cj.jdbc.Driver"));
    }
}